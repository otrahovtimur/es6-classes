// ТЕОРІЯ
/* 
1.Прототипне наслідування в JavaScript дозволяє об'єктам використовувати властивості та методи інших об'єктів через ланцюжок прототипів.

2.Виклик super() у конструкторі класу-нащадка потрібен для ініціалізації властивостей і методів батьківського класу.
*/

// ПРАКТИКА

class Employee {

    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name() {
        return this._name;
    }
    set name(newName) {
        newName = newName.trim();
        if (newName === '')
            console.log("Name cannot be empty");

        this._name = newName;
    }



    get age() {
        return this._age;
    }
    set age(newAge) {
        if (newAge <= 0)
            console.log("Invalid age");

        this._age = newAge;
    }

    get salary() {
        return this._salary;
    }
    set salary(newSalary) {
        if (newSalary <= 0)
            console.log("Invalid value");

        this._salary = newSalary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    get salary() {
        return this._salary * 3;
    }
}

const employee1 = new Programmer('Sofia', 40, 2700, "Spanish");
const employee2 = new Programmer('Andrew', 21, 3000, "English, Japanees");
const employee3 = new Programmer('David', 34, 2800, "Portugues, Spanish");
const employee4 = new Programmer('Pedrizio', 27, 3500, "Ukrainian, Italian, German");
const employee5 = new Programmer('Anna', 18, 1100, "French, Korean");

console.log('This is employee 1', employee1);
console.log('This is employee 1 salary', employee1.salary)

console.log('This is employee 2', employee2);
console.log('This is employee 2 salary', employee2.salary)

console.log('This is employee 3', employee3);
console.log('This is employee 3 salary', employee3.salary)

console.log('This is employee 4', employee4);
console.log('This is employee 4 salary', employee4.salary)

console.log('This is employee 5', employee5);
console.log('This is employee 5 salary', employee5.salary)
